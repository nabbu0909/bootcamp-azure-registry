# Bootcamp Azure Registry

This is a small test registry I made using the Azure platform

## Requirements

To push to this registry, you must be added as a contributer by the owner of the registry (me)

You must also install the Azure CLI. Instructions can be found [here](https://docs.microsoft.com/en-us/cli/azure/install-azure-cli)

## Push an image to the registry

1. First login to the Azure CLI with `az login`

2. Login to the registry by entering `az acr login --name narularegistry`

3. Tag the image you would like to push with `docker tag <image-name> narularegistry.azurecr.io/<image-name>:v1`

4. Push the image with `docker push narularegistry.azurecr.io/<image-name>:v1`

## Run an image from the registry

You can now run an image from the registry with `docker run narularegistry.azurecr.io/<image-name>:v1  `
